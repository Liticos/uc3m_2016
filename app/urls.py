from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^/$', views.top,         name='top'),
    url(r'^map/(?P<mapId>\w+)/$', views.view_map,      name='map'),
    url(r'^create/$', views.create,      name='create'),
    url(r'^api/poitypes/$', views.getPoiTypes,   name='poiTypes'),
    url(r'^api/pois/$', views.getPois,       name='pois'),
    url(r'^api/create/$', views.create_map,       name='create_map'),

]
