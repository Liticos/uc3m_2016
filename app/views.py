from django.shortcuts import render
from django.http import HttpResponse
import json
from bson import json_util
# Create your views here.
from app.Model import Model

def buildJsonResponse(content):
    return HttpResponse(json.dumps(content, default=json_util.default), content_type="application/json")

def getPoiTypes(request):
    return buildJsonResponse(Model().getTypes())

def getPois(request):
    id = request.GET["typeId"]
    res = Model().getPoisByType(int(id))
    return buildJsonResponse(res)

def index(request):
    return render(request, 'app/index.html')

def view_map(request, mapId):

    res = Model().getRoute(mapId)
    return render(request, 'app/view.html', res)

def create_map(request):
    data = request.GET
    res = Model().insertRoute(data["start"],
                              data["middle"],
                              data["end"],0,
                              data["uersName"],
                              data["routeName"],
                              data["tags"], 0)
    return buildJsonResponse({"result":res})

def create(request):
    return render(request, 'app/create.html')

def top(request):
    return render(request, 'app/list.html', {"res":Model.getTopRoutes(10)})

def webMap(request):
    path = {
        "init"      : [],
        "end"       : [],
        "waypoint"  :[[]]
    }
    return
