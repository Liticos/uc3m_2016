import pymongo as pm
from os import environ
import json
from pprint import pprint


class MongoConnector(object):
    def __init__(self):
       # self.client = pm.MongoClient(environ["MONGO_SERVICE"], environ["MONGO_PORT"])
       # self.db = self.client[self.cli]

       # self.client = pm.MongoClient("localhost", 27017)
       # self.db = self.client["hackathon"]

       self.client = pm.MongoClient("mongodb://ds059165.mongolab.com:59165/hackathon_uc3m")
       self.db = self.client["hackathon_uc3m"]
       self.db.authenticate(name="user", password="user")

    def initDataset(self):
        pois = json.loads(open("dataset/POIs.json").read())
        for poi in pois["poiList"]:
            poi = poi["attributes"]
            self.db["pois"].insert_one(poi)

        types = json.loads(open("dataset/POITypes.json").read())
        for type in types["types"]:
            type = type["attributes"]
            self.db["types"].insert_one(type)

    def insert(self, json, database):
        return self.db[database].insert_one(json).inserted_id

    def get(self, args, database):
        return self.db[database].find(args)

    def dropDatabase(self):
        self.client.drop_database("hackathon")

    def initialize(self):
        self.dropDatabase()
        self.initDataset()


if __name__ == "__main__":
    print("MongoConnector")
    mc = MongoConnector()
    print(mc.db)