from app.MongoConnector import MongoConnector as mc
from pprint import pprint
from bson.objectid import ObjectId

class Model:

    def __init__(self):
        self.connector = mc()

    def insertRoute(self, start, middle, end, rating, userName, routeName, tag, datetime):
        route = {
            "start"     : start,
            "end"       : end,
            "middle"    : middle,
            "rating"    : rating,
            "userName"  : userName,
            "routeName" : routeName,
            "tag"       : tag,
            "datetime"  : datetime
        }

        return self.connector.insert(route, "routes")


    def __getFullyQualifiedRoute(self, route):
        route["id"] = str(route["_id"])
        poiID = route["start"]
        route["start"] = [x for x in self.connector.get({"_id" : ObjectId(poiID)}, "pois")][0]
        poiID = route["end"]
        route["end"] = [x for x in self.connector.get({"_id" : ObjectId(poiID)}, "pois")][0]

        for x in range(0, len(route["middle"])):
            route["middle"][x] = [x for x in self.connector.get({"_id" : ObjectId(route["middle"][x])}, "pois")][0]

        return route

    def getRoute(self, id):
        routes = self.connector.get({"_id" : ObjectId(id)}, "routes")
        for route in routes:
            return self.__getFullyQualifiedRoute(route)

    def getRouteByTag(self, tag):
        cursor =  self.connector.get({"tag" : {"$in" : tag}}, "routes")
        routes = []
        for route in cursor:
            routes.append(self.__getFullyQualifiedRoute(route))

        return routes

    def getTopRoutes(self, limit):
        cursor = self.connector.db["routes"].aggregate\
        (
            [
                {'$sort' : {"rating" : -1}},
                {'$limit': limit}
            ]
        )

        routes = []
        for route in cursor:
            routes.append(self.__getFullyQualifiedRoute(route))

        return routes

    def getPoisByType(self, type):
        cursor = self.connector.get({"type" : type}, "pois")
        return [x for x in cursor]

    #AGGREGATE SORT NAME
    def getTypes(self):
        cursor = self.connector.db["types"].aggregate\
        (
            [
                {'$sort' : {"name" : 1}},
            ]
        )

        return [x for x in cursor]

if __name__ == "__main__":
    print("Model")
    model = Model()
    # document = model.insertRoute("56b5e63ad7ed4e59280242a6", ["56b5e63ad7ed4e59280242a5", "56b5e63ad7ed4e59280242a4"], "56b5e63ad7ed4e59280242a3", 11, "Juan Gomez", "Museos", ["Cultura", "Arte"], 0)
    # document = model.insertRoute("56b5e63ad7ed4e59280242a6", ["56b5e63ad7ed4e59280242a5", "56b5e63ad7ed4e59280242a4"], "56b5e63ad7ed4e59280242a3", 100, "Ana Rodriguez", "Restaurantes", ["Gastronomía", "Fiesta"], 0)
    # document = model.insertRoute("56b5e63ad7ed4e59280242a6", ["56b5e63ad7ed4e59280242a5", "56b5e63ad7ed4e59280242a4"], "56b5e63ad7ed4e59280242a3", 81, "Pedro Fernandez", "Fiestas", ["Fiesta"], 0)
    # document = model.insertRoute("56b5e63ad7ed4e59280242a6", ["56b5e63ad7ed4e59280242a5", "56b5e63ad7ed4e59280242a4"], "56b5e63ad7ed4e59280242a3", 57, "Alberto Garcia", "Parques", ["Cultura"], 0)
    # document = model.insertRoute("56b5e63ad7ed4e59280242a6", ["56b5e63ad7ed4e59280242a5", "56b5e63ad7ed4e59280242a4"], "56b5e63ad7ed4e59280242a3", 98, "Maria Luisa", "Monumentos", ["Cultura"], 0)
    # document = model.insertRoute("56b5e63ad7ed4e59280242a6", ["56b5e63ad7ed4e59280242a5", "56b5e63ad7ed4e59280242a4"], "56b5e63ad7ed4e59280242a3", 65, "Laura Villaverde", "Sitios", ["Cultura"], 0)



    # document = model.getPoisByType(3)
    pprint(model.getRoute("56b627a0d7ed4eb8983c8341"))
    # pprint(model.getTopRoutes(1))
    #pprint(document)
