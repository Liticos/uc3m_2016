import requests

url = "https://openbus.emtmadrid.es:9443/emt-proxy-server/last/geo/GetPointsOfInterest.php"
idClient = "WEB.SERV.alex93cabeza@gmail.com"
passKey = "19ED3446-BE1D-4699-89AD-A5863AE5E603"




POST = """
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:xsd="http://www.w3.org/2001/XMLSchema"
xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Body>
        <GetPointofInterest xmlns="http://tempuri.org/">
            <idClient>{idClient}</idClient>
            <passKey>{passKey}</passKey>
            <coordinateX>{xCoord}</coordinateX>
            <coordinateY>{yCoord}</coordinateY>
            <Radius>{radius}</Radius>
        </GetPointofInterest>
    </soap:Body>
</soap:Envelope>
"""


POST = POST.format(**{'idClient' : idClient,
             'passKey'  : passKey,
             'xCoord'   : -3.703548,
             'yCoord'   : 40.416937,
             'radius'   : 20000})

# headers = {'Content-Type': 'application/xml'}

headers = {'Content-Type': 'text/xml; charset=utf-8',
           'Content-Length': len(POST),
           'Host': 'localhost',
           'SOAPAction': "http://tempuri.org/ GetPointofInterest "}



if __name__ == "__main__":
    pass
    # print(POST)
    # print("\n\n\n")
    # print(headers)
    #
    #
    #
    #
    # output = requests.post(url, data = POST, headers = headers, verify=False).text
    # print (output)